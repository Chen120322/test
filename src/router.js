import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes'
import setWechatTitle from './utils/setwechattitle'


//注册使用router

Vue.use(VueRouter);

//实例化router对象

const router = new VueRouter({
  routes
});



router.beforeEach((to, from, next) => {
  // ...
  console.log('路由跳转前');
  //在这里判断用户状态是否在登录，若是，向后端发出登录？
  let loginToken = window.sessionStorage.getItem('loginToken');

  console.log(loginToken);

  if(to.path != '/login' &&　to.path != '/logout'){
    if(loginToken){
      console.log('登录信息还存在');
      next();
    }else{
      console.log(1)
      next();
    }
  }else{
    next();
  }


  /*  if(to.path != '/login' &&　to.path != '/logout' && store.state.isLogin == false ){
   if(userName && userPsw){
   console.log('有用户名');
   Vue.http.post('/login', {
   account: window.localStorage.getItem('account'),
   password:  window.localStorage.getItem('password')
   })
   .then(function (response) {
   const data = response.data;
   if(data.httpCode==200){
   store.dispatch('login');
   router.push({ path: to.path });

   //以上自动登录的处理暂时可以，就是后端的用户还是登录状态再次登录会出问题导致目前效果紊乱
   }else{
   next({
   path: '/login'
   });
   }
   });

   }else{
   console.log('需要登录');
   next({
   path: '/login'
   });
   }
   }else{*/
  //next();
  //}
});







router.afterEach(route => {
  // ...
    let title = route.meta.title + '-嘟嘟自驾'
    setWechatTitle(title);
  console.log('路由跳转后')
});


//导出router对象
export default router
