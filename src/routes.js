import Login from './components/Login.vue'
import Home from './components/Home.vue'
import dept from './components/dept/List.vue'
import menu from './components/menu/List.vue'
import role from './components/role/List.vue'
import session from './components/session/List.vue'
import dic from './components/dic/List.vue'


//易捷

import YJProduct from './components/yijie/product/List.vue'
import YJCardBatch from './components/yijie/batch/List.vue'
import YJCard from './components/yijie/card/List.vue'

import project from './components/project/List.vue'
import projectAdd from './components/project/Add.vue'
import projectPrice from './components/project/Price.vue'
import projectUpdate from './components/project/Update.vue'
import projectPreview from './components/project/Preview.vue'
import Trip from './components/trip/List.vue'
import TripAdd from './components/trip/Add.vue'
import TripPerfect from './components/trip/Perfect.vue'
import TripUpdate from './components/trip/Update.vue'
import TripPreview from './components/trip/Preview.vue'
import TripSiteAdd from './components/tripsite/Add.vue'

import menuAdd from './components/menu/Add.vue'
import Map from './components/Map.vue'
import Operator from './components/operator/List.vue'
import Banner from './components/banner/List.vue'
import DoLog from './components/log/DoList.vue'
import LoginLog from './components/log/LoginList.vue'
import Nav from './components/nav/List.vue'
import supplier from './components/supplier/List.vue'
import SupType from './components/supplier/TypeList.vue'
import SupAccount from './components/supplier/AccountList.vue'
import product from './components/product/List.vue'
import ProClassify from './components/product/Classify.vue'
import ProType from './components/product/TypeList.vue'
import ProUpdate from './components/product/Update.vue'
import ProAdd from './components/product/Add.vue'
import ProPreview from './components/product/Preview.vue'
import ProPrice from './components/product/Price.vue'
import Welcome from './components/welcome/Index.vue'
import Help from './components/welcome/Help.vue'
import ChangeLog from './components/welcome/ChangeLog.vue'
import Refund from './components/refund/List.vue'
import CustomList from './components/custom/List.vue'

//协会
import ClubProduct from './components/association/product/List.vue'
import ClubActivities from './components/association/activities/List.vue'
import ClubStaff from './components/association/staff/List.vue'
import ClubStaffCommission from './components/association/staff/Commission.vue'
import VipOrder from './components/association/VipOrder.vue'

//wkq 定制
import ChooseCity from  './components/custom/ChooseCity.vue'
import CustomAdd from  './components/custom/Add.vue'
import CusTripList from  './components/custom/TripList.vue'

//wkq 营销活动
import newYear2018 from  './components/promotion/20180101/OrderList.vue'
import newYear2018Code from  './components/promotion/20180101/Code.vue'
import wm from  './components/promotion/wm/List.vue'   //38女神表单
import PromotionIndex from './components/promotion/Index.vue'

// kws
import LocationMan from './components/locationManag/LocationMan.vue'
import Coupon from './components/coupon/Coupon.vue'
import CouponLinkPlay from './components/coupon/CouponLinkPlay.vue'
import SendCoupon from './components/coupon/SendCoupon.vue'
import Complaints from './components/complaints/Complaints.vue'
import Couponsend from './components/couponsend/Couponsend.vue'
import FinalAccounts from './components/finalAccounts/FinalAccounts.vue'
import Preference from './components/preference/Preference.vue'
import Theme from './components/theme/Theme.vue'
import Chat from './components/chat/Chat.vue'
import LocSort from './components/LocSort/LocSort.vue'
import UserAttr from './components/UserAttr/UserAttr.vue'
import User from './components/user/User.vue'
import ProductOrders from './components/ProductOrders/ProductOrders.vue'
import PayOrder from './components/ProductOrders/PayOrder.vue'
import Activity from './components/activity/Activity.vue'
import ActivityAdd from './components/activity/ActivityAdd.vue'
import ActivityEdit from './components/activity/ActivityEdit.vue'
import ActivityLinkPlay from './components/activity/activityLinkPlay.vue'
import ActivityLinkUser from './components/activity/activityLinkUser.vue'
import Schedule from './components/schedule/Schedule.vue'
import ScheduleLog from './components/schedule/ScheduleLog.vue'
import GroupList from './components/groups/GroupList.vue'
import SinoList from './components/yijie/sino/SinoOrder.vue'
import FinanceList from './components/yijie/sino/FinanceOrder.vue'
import MotionList from './components/yijie/sino/MotionOrder.vue'
import SelfDriveList from './components/yijie/seldrive/List.vue'
import Association from './components/association/list.vue'
import LinkSinoCard from './components/association/LinkSinoCard.vue'
import AstList from './components/association/AssociationList.vue'

import userRankList from './components/userRank/List.vue'

const routes = [
    {
        path: '/map',
        component: Map,
        hidden: true,
        meta: {
            title: '地图DEMO'
        },
    },
    {
        path: '/',
        component: Home,
        hidden: true,
        meta: {
            title: '首页'
        },
        children: [
            {//欢迎页面
                name: 'welcome',
                path: '/index',
                component: Welcome,
                meta: {
                    title: '运营系统'
                },
            },
            {//帮助页面
              name: 'help',
              path: '/help',
              component: Help,
              meta: {
                title: '帮助'
              },
            },
            {//帮助页面
              name: 'changelog',
              path: '/changelog',
              component: ChangeLog,
              meta: {
                title: '更新日志'
              },
            },


            {//部门列表页
                name: 'deptlist',
                path: 'sys/dept/list/:pageNum/:pageSize',
                component: dept,
                meta: {
                    title: '部门管理'
                },
            },
            {//菜单列表页
                name: 'menulist',
                path: 'sys/menu/list/:pageNum/:pageSize',
                component: menu,
                meta: {
                    title: '菜单管理'
                },
            },
            {//角色列表页
                name: 'rolelist',
                path: '/sys/role/:pageNum/:pageSize',
                component: role,
                meta: {
                    title: '角色管理'
                },
            },
            {//会话列表页
                name: 'sessionlist',
                path: 'sys/session/list/:pageNum/:pageSize',
                component: session,
                meta: {
                    title: '会话管理'
                },
            },
            {//字典列表页
                name: 'diclist',
                path: 'sys/dic/list/:pageNum/:pageSize',
                component: dic,
                meta: {
                    title: '字典管理'
                },
            },

            //2017-5-3 new job

            {//供应商管理列表
                name: 'supplierlist',
                path: 'supplier/select/list/:pageNum/:pageSize',
                component: supplier,
                meta: {
                    title: '供应商管理'
                },
            },
            {//供应商类型管理列表
                name: 'suptype',
                path: 'supplier/supplierType/getIdList/:pageNum/:pageSize',
                component: SupType,
                meta: {
                    title: '供应商类型管理'
                },
            },
            {//供应商账户列表
              name: 'supaccount',
              path: 'supplierAccount/supplierAccount/list/:pageNum/:pageSize',
              component: SupAccount,
              meta: {
                title: '供应商账号管理'
              },
            },

            {//产品退款
              name: 'refund',
              path: 'dudu/refund/:pageNum/:pageSize',
              component: Refund,
              meta: {
                title: '退款管理'
              },
            },


            {//产品分类管理管理列表
                name: 'proclassify',
                path: '/dudu/itemSort/:pageNum/:pageSize',
                component: ProClassify,
                meta: {
                    title: '产品分类管理'
                },
            },

            {//产品类型管理管理列表
                name: 'protype',
                path: '/dudu/itemType/:pageNum/:pageSize',
                component: ProType,
                meta: {
                    title: '产品类型管理'
                },
            },

            {//产品管理列表
                name: 'dudu/product',
                path: 'dudu/product/:pageNum/:pageSize',
                component: product,
                meta: {
                    title: '产品管理'
                },
            },
            {//编辑产品
                name: 'proupdate',
                path: '/dudu/update/product/',
                component: ProUpdate,
                meta: {
                    title: '产品编辑'
                },
            },
            {//编辑产品
                name: 'proadd',
                path: '/dudu/add/product/',
                component: ProAdd,
                meta: {
                    title: '新增产品'
                },
            },
            {//预览产品
              name: 'propreview',
              path: '/dudu/preview/product/',
              component: ProPreview,
              meta: {
                title: '预览产品'
              },
            },



            {//编辑产品价格
                name: 'proprice',
                path: '/dudu/price/product/',
                component: ProPrice,
                meta: {
                    title: '产品日期与价格'
                },
            },
            {//项目管理列表
                name: 'projectlist',
                path: 'dudu/item/:pageNum/:pageSize',
                component: project,
                meta: {
                    title: '项目管理'
                },
            },
            {//增加项目
                name: 'projectadd',
                path: 'dudu/add/item',
                component: projectAdd,
                meta: {
                    title: '新增项目'
                },
            },
          {//项目价格修改
            name: 'projectprice',
            path: 'dudu/price/item',
            component: projectPrice,
            meta: {
              title: '项目价格修改'
            },
          },
          {//项目编辑
            name: 'projectupdate',
            path: 'dudu/update/item',
            component: projectUpdate,
            meta: {
              title: '项目编辑'
            },
          },
          {//项目编辑
            name: 'projectpreview',
            path: 'dudu/preview/item',
            component: projectPreview,
            meta: {
              title: '项目预览'
            },
          },
            {//玩法管理列表
                name: 'playlist',
                path: 'dudu/trip/:pageNum/:pageSize',
                component: Trip,
                meta: {
                    title: '玩法管理'
                },
            },
            {//增加玩法
                name: 'tripadd',
                path: 'dudu/add/trip',
                component: TripAdd,
                meta: {
                    title: '新增玩法'
                },
            },
            {//增加玩法
                name: 'tripperfect',
                path: 'dudu/perfect/trip',
                component: TripPerfect,
                meta: {
                    title: '完善玩法'
                },
            },
            {//编辑玩法
              name: 'tripupdate',
              path: 'dudu/update/trip',
              component: TripUpdate,
              meta: {
                title: '玩法编辑'
              },
            },
            {//玩法预览
              name: 'trippreview',
              path: 'dudu/preview/trip',
              component: TripPreview,
              meta: {
                title: '玩法预览'
              },
            },
          {//增加站点
            name: 'siteadd',
            path: 'dudu/add/tripsite',
            component: TripSiteAdd,
            meta: {
              title: '新增站点'
            },
          },
            {//操作员列表
                name: 'operator',
                path: 'sys/operator/:pageNum/:pageSize',
                component: Operator,
                meta: {
                    title: '操作员管理'
                },
            },

            {//banner图管理列表
                name: 'banner',
                path: 'sys/banner/:pageNum/:pageSize',
                component: Banner,
                meta: {
                    title: '首页广告管理'
                },
            },
            {//操作员操作日志
                name: 'operatorLog',
                path: 'log/operatorLog/:pageNum/:pageSize',
                component: DoLog,
                meta: {
                    title: '操作员操作日志'
                },
            },
            {//操作员登录日志
                name: 'operatorLoginLog',
                path: '/log/operatorLoginLog/:pageNum/:pageSize',
                component: LoginLog,
                meta: {
                    title: '操作员登录日志'
                },
            },
            {//导航模块
                name: 'nav',
                path: '/sys/model/:pageNum/:pageSize',
                component: Nav,
                meta: {
                    title: '导航模块'
                },
            },

            //新增
            {//菜单新增页
                name: 'menuadd',
                path: 'sys/add/menu',
                component: menuAdd
            },


          //营销活动~~~2018



          {//定制列表管理
            name: 'promotion',
            path: '/sinopec',
            component: PromotionIndex,
            meta: {
              title: '第三方活动管理'
            },
            children:[
              {
                name:'newyear2018',
                path: 'order/list/:pageNum/:pageSize',
                component: newYear2018,
                meta: {
                  title: '兑换订单列表'
                },
              },
              {
                name:'newyear2018code',
                path: 'code/list/:pageNum/:pageSize',
                component: newYear2018Code,
                meta: {
                  title: '兑换码管理'
                },
              },
            ]
          },


          {
            name:'wm',
            path: '/goddessActivity/read/list/:pageNum/:pageSize',
            component: wm,
            meta: {
              title: '3.8女神活动管理'
            },
          },


          //定制

          {//定制列表管理
            name: 'custom',
            path: 'cus/order/:pageNum/:pageSize',
            component: CustomList,
            meta: {
              title: '定制管理'
            },
          },

          {//定制选择城市
            name: 'chooseCity',
            path: 'cus/choosecity',
            component: ChooseCity,
            meta: {
              title: '选择出发地和目的地'
            },
          },

          {//定制添加编辑
            name: 'customadd',
            path: 'cus/add',
            component: CustomAdd,
            meta: {
              title: '定制线路'
            },
          },

          {//定制线路列表
            name: 'cuslist',
            path: 'cus/triplist/:pageNum/:pageSize',
            component: CusTripList,
            meta: {
              title: '定制管理'
            },
          },




          //易捷

          {//易捷产品功能
            name: 'yjproduct',
            path: 'yj/product/:pageNum/:pageSize',
            component: YJProduct,
            meta: {
              title: '易捷产品管理'
            },
          },

          {//易捷实体卡批次
            name: 'yjcardbatch',
            path: 'yj/cardbatch/:pageNum/:pageSize',
            component: YJCardBatch,
            meta: {
              title: '易捷实体卡批次管理'
            },
          },

          {//易捷实体卡管理
            name: 'yjcard',
            path: 'yj/card/:pageNum/:pageSize',
            component: YJCard,
            meta: {
              title: '易捷实体卡管理'
            },
          },


          //协会

          {//协会产品管理
            name: 'clubproduct',
            path: 'ast/astProduct/:pageNum/:pageSize',
            component: ClubProduct,
            meta: {
              title: '协会产品管理'
            }
          },

          {//协会活动管理
            name: 'clubActivities',
            path: 'ast/astActivity/:pageNum/:pageSize',
            component: ClubActivities,
            meta: {
              title: '协会活动管理（一元活动）'
            }
          },

          {//协会活动管理
            name: 'VipOrder',
            path: 'ast/associationOrder/:pageNum/:pageSize',
            component: VipOrder,
            meta: {
              title: '会员会费支付订单管理'
            }
          },


          {//协会兼职管理
            name: 'clubStaff',
            path: 'ast/aststaff/:pageNum/:pageSize',
            component: ClubStaff,
            meta: {
              title: '协会兼职人员管理'
            }
          },


          {//协会兼职佣金记录
            name: 'basicsClearing',
            path: 'ast/basicsClearing/:pageNum/:pageSize',
            component: ClubStaffCommission,
            meta: {
              title: '协会兼职佣金结算管理'
            }
          },

           

          






            //重定向

            {path: 'sinopec/order/list', redirect: '/sinopec/order/list/1/20'},  //重定向至第一页
            {path: 'sinopec/code/list', redirect: '/sinopec/code/list/1/20'},  //重定向至第一页
            {path: 'goddessActivity/read/list', redirect: '/goddessActivity/read/list/1/20'},  //重定向至第一页
            
            {path: 'ast/aststaff/', redirect: '/ast/aststaff/1/20'},  //重定向至第一页
            {path: 'ast/basicsClearing/', redirect: '/ast/basicsClearing/1/20'},  //重定向至第一页
            {path: 'ast/astProduct/', redirect: '/ast/astProduct/1/20'},  //重定向至第一页
            

            
            {path: 'sys/activitylinkuser/', redirect: '/sys/activitylinkuser/1/20'},  //重定向至第一页
            {path: 'sys/activitylinkplay/', redirect: '/sys/activitylinkplay/1/20'},  //重定向至第一页
            {path: 'sys/sendcoupon/', redirect: '/sys/sendcoupon/1/20'},  //重定向至第一页
            {path: 'sys/couponLinkPlay/', redirect: '/sys/couponLinkPlay/1/20'},  //重定向至第一页
            {path: 'ast/associationOrder/', redirect: '/ast/associationOrder/1/20'},  //重定向至第一页
            {path: 'ast/astActivity/', redirect: '/ast/astActivity/1/20'},  //重定向至第一页
            {path: 'cus/triplist/', redirect: '/cus/triplist/1/20'},  //重定向至第一页
            {path: 'cus/order/', redirect: '/cus/order/1/20'},  //重定向至第一页
            {path: 'yj/card/', redirect: 'yj/card/1/20'},  //重定向至第一页
            {path: 'yj/product', redirect: 'yj/product/1/20'},  //重定向至第一页
            {path: 'yj/cardbatch', redirect: 'yj/cardbatch/1/20'},  //重定向至第一页
            {path: 'dudu/itemSort', redirect: 'dudu/itemSort/1/20'},  //重定向至第一页
            {path: 'dudu/itemType', redirect: '/dudu/itemType/1/20'},  //重定向至第一页
            {path: 'sys/user/list', redirect: 'sys/user/list/1/20'},  //重定向至第一页
            {path: 'sys/dept/list', redirect: 'sys/dept/list/1/20'},  //重定向至第一页
            {path: 'sys/menu/list', redirect: 'sys/menu/list/1/20'},  //重定向至第一页
            {path: 'sys/role', redirect: '/sys/role/1/20'}, //重定向至第一页
            {path: 'sys/session/list', redirect: 'sys/session/list/1/20'},  //重定向至第一页
            {path: 'sys/dic/list', redirect: 'sys/dic/list/1/20'},  //重定向至第一页
            {path: 'supplier/select/list', redirect: 'supplier/select/list/1/20'},  //重定向至第一页
            {path: 'supplier/supplierType/getIdList', redirect: 'supplier/supplierType/getIdList/1/20'},  //重定向至第一页
            {path: 'supplierAccount/supplierAccount/list', redirect: 'supplierAccount/supplierAccount/list/1/20'},  //重定向至第一页\
            {path: 'dudu/product', redirect: 'dudu/product/1/20'},  //重定向至第一页
            {path: 'dudu/item', redirect: 'dudu/item/1/20'},  //重定向至第一页
            {path: 'dudu/trip', redirect: 'dudu/trip/1/20'},  //重定向至第一页
            {path: 'dudu/refund', redirect: 'dudu/refund/1/20'},  //重定向至第一页
            {path: 'sys/operator', redirect: 'sys/operator/1/20'},  //重定向至第一页
            {path: 'sys/banner', redirect: 'sys/banner/1/20'},  //重定向至第一页
            {path: 'log/operatorLog', redirect: 'log/operatorLog/1/20'},  //重定向至第一页
            {path: 'log/operatorLoginLog', redirect: 'log/operatorLoginLog/1/20'},  //重定向至第一页
            {path: 'sys/model', redirect: 'sys/model/1/20'},  //重定向至第一页
            {path: 'site/selectSite/list', redirect: 'site/selectSite/list/1/20'},  //重定向至第一页
            {path: 'site/siteSort/list', redirect: 'site/siteSort/list/1/20'},  //重定向至第一页
            {path: 'dudu/kefu', redirect: 'dudu/kefu/1/20'},  //重定向至第一页
            {path: 'userAttributes/userAttr/list', redirect: 'userAttributes/userAttr/list/1/20'},  //重定向至第一页
            {path: 'tripTopic/tripTopic/list', redirect: 'tripTopic/tripTopic/list/1/20'},  //重定向至第一页
            {path: 'tag/tag/list', redirect: 'tag/tag/list/1/20'},  //重定向至第一页
            {path: 'user/user/list', redirect: 'user/user/list/1/20'},  //重定向至第一页
            {path: 'dudu/productOrder', redirect: 'dudu/productOrder/1/20'},  //重定向至第一页
            {path: 'dudu/userPayOrder', redirect: 'dudu/userPayOrder/1/20'},  //重定向至第一页
            {path: 'dudu/activity', redirect: 'dudu/activity/1/20'},  //重定向至第一页
            {path: 'dudu/hongbao', redirect: 'dudu/hongbao/1/20'},  //重定向至第一页
            {path: 'task/schedule', redirect: 'task/schedule/1/20'},  //重定向至第一页
            {path: 'dudu/tripTeam', redirect: 'dudu/tripTeam/1/20'},  //重定向至第一页
            {path: 'yj/cardOrderOperation', redirect: 'yj/cardOrderOperation/1/20'},  //重定向至第一页
            {path: 'yj/cardOrderSinopec', redirect: 'yj/cardOrderSinopec/1/20'},  //重定向至第一页
            {path: 'yj/cardOrderFinance', redirect: 'yj/cardOrderFinance/1/20'},  //重定向至第一页
            {path: 'yj/yjTravelProduct', redirect: 'yj/yjTravelProduct/1/20'},  //重定向至第一页
            {path: 'ast/associationUser', redirect: 'ast/associationUser/1/20'},  //重定向至第一页
            {path: 'ast/userOil', redirect: 'ast/userOil/1/20'},  //重定向至第一页
            {path: 'ast/astOrder', redirect: 'ast/astOrder/1/20'},  //重定向至第一页

            {path: 'userRank/List', redirect: 'userRank/List/1/20'},  //重定向至第一页
                //kws
               {//地点管理
                name: 'locationManage',
                path: 'site/selectSite/list/:pageNum/:pageSize',
                component: LocationMan,
                meta: {
                    title: '地点管理'
                },

            },{//活动管理
                name: 'activities',
                path: 'dudu/activity/:pageNum/:pageSize',
                component: Activity,
                meta: {
                    title: '活动管理'
                },
            },{//活动添加
                name: 'activityAdd',
                path: 'sys/activityadd',
                component: ActivityAdd,
                meta: {
                    title: '添加活动'
                },
            },{//活动编辑
                name: 'activityEdit',
                path: 'sys/activityedit',
                component: ActivityEdit,
                meta: {
                    title: '编辑活动'
                },
            },
            {//活动关联玩法
                name: 'activityLinkPlay',
                path: 'sys/activitylinkplay/:pageNum/:pageSize',
                component: ActivityLinkPlay,
                meta: {
                    title: '指定玩法'
                },
            },{//活动关联用户
                name: 'activityLinkUser',
                path: 'sys/activitylinkuser/:pageNum/:pageSize',
                component: ActivityLinkUser,
                meta: {
                    title: '指定用户'
                },
            },
            {//投诉处理
                name: 'complaints',
                path: 'sys/complaints/:pageNum',
                component: Complaints,
                meta: {
                    title: '投诉处理'
                },
            },{//红包
                name: 'coupon',
                path: 'dudu/hongbao/:pageNum/:pageSize',
                component: Coupon,
                meta: {
                    title: '红包'
                },
            },
            {//活动关联玩法
                name: 'couponLinkPlay',
                path: 'sys/couponLinkPlay/:pageNum/:pageSize',
                component: CouponLinkPlay,
                meta: {
                    title: '红包指定玩法'
                },
            },
            {//发放红包
                name: 'sendcoupon',
                path: 'sys/sendcoupon/:pageNum/:pageSize',
                component: SendCoupon,
                meta: {
                    title: '发放红包'
                },
            },
            {//红包发放couponsend
                name: 'couponsend',
                path: 'sys/couponsend/:pageNum',
                component: Couponsend,
                meta: {
                    title: '红包发放'
                },
            },{//结算
                name: 'finalAccounts',
                path: 'sys/finalAccounts/:pageNum',
                component: FinalAccounts,
                meta: {
                    title: '结算'
                },
            },{//偏好标签
                name: 'preference',
                path: 'tag/tag/list/:pageNum/:pageSize',
                component: Preference,
                meta: {
                    title: '偏好标签'
                },
            },{//主题管理
                name: 'theme',
                path: 'tripTopic/tripTopic/list/:pageNum/:pageSize',
                component: Theme,
                meta: {
                    title: '主题管理'
                },
            },
            {//客服管理
                name: 'chat',
                path: 'dudu/kefu/:pageNum/:pageSize',
                component: Chat,
                meta: {
                    title: '客服管理'
                },
            },{//地点分类管理
                name: 'locSort',
                path: 'site/siteSort/list/:pageNum/:pageSize',
                component: LocSort,
                meta: {
                    title: '地点分类'
                },
            },{//用户属性管理
                name: 'UserAttr',
                path: 'userAttributes/userAttr/list/:pageNum/:pageSize',
                component: UserAttr,
                meta: {
                    title: '用户属性管理'
                },
            },
            {//用户管理
                name: 'User',
                path: 'user/user/list/:pageNum/:pageSize',
                component: User,
                meta: {
                    title: '用户管理'
                },
            },{//产品订单
                name: 'productOrders',
                path: 'dudu/productOrder/:pageNum/:pageSize',
                component: ProductOrders,
                meta: {
                    title: '产品订单'
                },
            },{//支付订单
                name: 'PayOrder',
                path: 'dudu/userPayOrder/:pageNum/:pageSize',
                component: PayOrder,
                meta: {
                    title: '支付订单'
                },
            },
            {//调度
                name: 'Schedule',
                path: 'task/schedule/:pageNum/:pageSize',
                component: Schedule,
                meta: {
                    title: '调度管理'
                },
            },
           /* {//调度记录
                name: 'scheduleLog',
                path: 'task/schedule/:pageNum',
                component: ScheduleLog,
                meta: {
                    title: '调度任务记录'
                },
            },*/
            {
                name: 'sinoOrder',
                path: 'yj/cardOrderSinopec/:pageNum/:pageSize',
                component: SinoList,
                meta: {
                    title: '订单明细表——石化'
                },
            },
            {
                name: 'financeList',
                path: 'yj/cardOrderFinance/:pageNum/:pageSize',
                component: FinanceList,
                meta: {
                    title: '实体卡订单管理——财务'
                },
            },
            {
                name: 'motionList',
                path: 'yj/cardOrderOperation/:pageNum/:pageSize',
                component: MotionList,
                meta: {
                    title: '实体卡订单管理——运营'
                },
            },
            {
                name: 'grouplist',
                path: 'dudu/tripTeam/:pageNum/:pageSize',
                component: GroupList,
                meta: {
                    title: '实体卡订单管理——运营'
                },
            },
            {
                name: 'seldrive',
                path: 'yj/yjTravelProduct/:pageNum/:pageSize',
                component: SelfDriveList,
                meta: {
                    title: '易捷自驾游产品管理'
                },
            },
            {
                name: 'association',
                path: 'ast/associationUser/:pageNum/:pageSize',
                component: Association,
                meta: {
                    title: '协会会员管理'
                },
            },
            {
                name: 'linksinocard',
                path: 'ast/userOil/:pageNum/:pageSize',
                component: LinkSinoCard,
                meta: {
                    title: '绑定加油卡信息'
                },
            },
            {
                name: 'associationlist',
                path: 'ast/astOrder/:pageNum/:pageSize',
                component: AstList,
                meta: {
                    title: '协会订单管理'
                },
            },
              {//用户管理
                name: 'userRank',
                path: 'userRank/List/:pageNum/:pageSize',
                component: userRankList,
                meta: {
                    title: '用户管理'
                },
            }

        ]
    },
    {
        path: '/login',
        component: Login,
        hidden: true,
        meta: {
            title: '登录'
        },
    }
];


export default routes;
