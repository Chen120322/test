import Vue from  'vue'
import Vuex from 'vuex'

Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        host:'http://res.17dd.com/',  //图片资源域名
        isLogin: false,   //是否登录的状态
        loading: false,    //loadding动画
        bell: 0,            //消息数量
        needLogin: false,


        //常规校验正则
        telReg:/^((0\d{2,4})-)(\d{6,8})(-(\d{3,}))?$/,   //固话
        phoneReg:/^1[3|4|5|6|7|8|9][0-9]{9}$/, //手机
        emailReg:/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,  //email
        spaceReg:/\s+/g, //空格
        urlReg:/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?/, //url
        abcNumReg:/^[a-zA-Z][a-zA-Z0-9]*$/, //字母、或者字母+数字的结合，字母开头
        priceReg: /(^[0-9]{1,19}$)|(^[0-9]{1,16}[\.]{1}[0-9]{1,2}$)/,  //总共不超过20位，只能2位小数点。
        numberReg: /^[0-9]{1,20}$/,  //总共不超过20位整数。
        idCardEeg: /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/,   //简单的15/18位身份证
    },
    mutations: {
        login: function (state) {
          state.isLogin = true;
          state.needLogin = false;
        },
        logout:function (state) {
          state.isLogin = false;
          state.needLogin = true;
        },
        loading: state => state.loading = true,
        loadComplete: state => state.loading = false,
    },
    actions: {
        login ({commit}) {
            commit('login')
        },
        logout ({commit}) {
            commit('logout')
        },
        closeLoading({commit}) {
          commit('loadComplete')
        },
    }
});
