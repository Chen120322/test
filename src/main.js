// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import $ from 'jquery'


import VueResource from 'vue-resource'
import store from './vuex/store'
import router from './router'

require('font-awesome/css/font-awesome.css')

Vue.use(ElementUI);
Vue.use(Vuex);
Vue.use(VueResource);
Vue.use(VueAwesomeSwiper);



Vue.http.options.emulateJSON = true;  //定义传输格式

//添加inteceptor
Vue.http.interceptors.push(function (request, next) {

  //store.commit('loading'); //开始loadding动画

  next(function (response) {

    //store.dispatch('closeLoading');  //停止loadding动画
    console.log(response)
    //统一定义，后端提示的未登录401需要跳转到login登录
    if(response.data.code == 401){
      app.$message({
        message: response.data.msg,
        type: 'error'
      });

      //不跳转回登录页，避免已经填写的表单丢失，设置vuex状态使得登录框出现
      store.commit('logout');
      console.log(router.currentRoute.path)
      if(router.currentRoute.path === '/'){  //如果是根路径，未登录的话，跳到登录页
         setTimeout(function () {
         router.replace('/login');
         },1000)
      }

    }

    if(response.body.code == 403){
      app.$message({
        message: response.body.code.msg,
        type: 'error'
      });

      //不跳转回登录页，避免已经填写的表单丢失，设置vuex状态使得登录框出现
      store.commit('logout');
      console.log(router.currentRoute.path)
      if(router.currentRoute.path === '/'){  //如果是根路径，未登录的话，跳到登录页
        setTimeout(function () {
          router.replace('/login');
        },1000)
      }
      return false;
    }else{

      return response

    }

    if(response.status == 207){
      app.$message({
        message: '你动作太快，连续操作间隔时间不能少于1秒',
        type: 'error'
      });
      return false;
    }else{

      return response

    }

    //console.log(store.state.loading)
  });
});




var app = new Vue({
  el: '#app',
  template: '<App/>',
  router,
  store,
  render: h => h(App)
}).$mount('#app');










