//站点管理
import Vue from 'vue'

const listUrl = '/tripSite/read/lis'; //获取列表

export default {
  getList (data, callback){
    Vue.http.post(listUrl, data)
      .then((response) => {
        callback(response)
      })
      .catch(function(response) {
        console.log(response)
      });
  },
  add () {

  }
}
