//操作员登录日志
import Vue from 'vue'

const listUrl = '/log/read/operatorLoginLog'; //获取用户的列表

export default {
    getList (data, callback){
        Vue.http.post(listUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    }
}
