//部门接口
//import reqwest from '../../static/reqwest.min';
import Vue from 'vue'

const listUrl = '/supplier/supplierType/getIdList'; //获取部门的列表
const addUrl = '/supplier/supplierType/insert'; //添加
const editUrl = '/supplier/supplierType/update'; //修改
const delUrl = '/supplier/supplierType/delete'; //删除

export default {
    getList (data, callback){
        Vue.http.post(listUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    add (data, callback){
        Vue.http.post(addUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    update (data, callback){
        Vue.http.post(editUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    del (data, callback){
        Vue.http.post(delUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
}
