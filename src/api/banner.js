//部门接口
//import reqwest from '../../static/reqwest.min';
import Vue from 'vue'

const listUrl = '/banner/read/list'; //获取banner的列表

export default {
    getList (data, callback){
        Vue.http.post(listUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    add () {

    }
}
