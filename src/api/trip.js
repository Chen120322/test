import Vue from 'vue'

const listUrl = '/trip/read/list'; //获取列表
const addUrl = '/trip/add'; //新增玩法
const addUIUrl = '/trip/addUI' //新增玩法之前查询
const editUrl = '/trip/update'; //修改
const updateUIUrl = '/trip/updateUI' //修改玩法之前查询
const delUrl = '/trip/delete'; //删除玩法
const statusUrl = '/itemType/disable'; //状态
const downUrl = '/trip/downTrip'; //下架玩法
const upUrl = '/trip/upTrip'; //上架玩法
const upDownUrl = '/trip/upOrDownUI'; //上下架之前查询是否可以上下架



export default {
    getList (data, callback){
        Vue.http.post(listUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    add (data, callback, catchError){
        Vue.http.post(addUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                catchError(response)
            });
    },
    update (data, callback){
        Vue.http.post(editUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    del (data, callback){
        Vue.http.post(delUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    state (data, callback){
        Vue.http.post(statusUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    getAddUI (data, callback){
        Vue.http.post(addUIUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    getUpdateUI (data, callback){
        Vue.http.post(updateUIUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
}
