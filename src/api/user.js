//用户接口
//import reqwest from '../../static/reqwest.min';
import Vue from 'vue'

const listUrl = '/operator/getMenu'; //获取用户的列表
const addUrl = ''; //添加用户
const updateUrl = '/user/update'; //更新用户
const updatePswUrl = '/user/update/password' //修改密码

export default {
  getList (data, callback){
    Vue.http.post(listUrl, data)
      .then((response) => {
        callback(response)
      })
      .catch(function(response) {
        console.log(response)
      });
  },
  add () {

  }
}
