//字典接口
import Vue from 'vue'

const listUrl = '/dic/read/list'; //获取用户的列表

export default {
  getList (data, callback){
    Vue.http.post(listUrl, data)
      .then((response) => {
        callback(response)
      })
      .catch(function(response) {
        console.log(response)
      });
  }
}
