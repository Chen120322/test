//部门接口
//import reqwest from '../../static/reqwest.min';
import Vue from 'vue'

const listUrl = '/itemSort/read/list'; //获取列表
const addUrl = '/itemSort/add'; //添加
const editUrl = '/itemSort/update'; //修改
const delUrl = '/itemSort/delete'; //删除
const statusUrl = '/itemSort/disable'; //状态

export default {
    getList (data, callback){
        Vue.http.post(listUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    add (data, callback){
        Vue.http.post(addUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    update (data, callback){
        Vue.http.post(editUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    del (data, callback){
        Vue.http.post(delUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
    state (data, callback){
        Vue.http.post(statusUrl, data)
            .then((response) => {
                callback(response)
            })
            .catch(function(response) {
                console.log(response)
            });
    },
}
